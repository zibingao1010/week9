# Streamlit App with a Hugging Face Model

Link to my app: https://ids721-week9-9zicayrbzp9ess6oaswtsq.streamlit.app/

## Steps

1. Create a Python script `app.py` that inclues your functionality of the app.

2. Create a `requirements.txt` file that includes all the dependencies. For mine, I have the following: 

``` bash
streamlit 
transformers
tensorflow
tf-keras
```

3. To deploy the website onto Streamlit, first push `app.py` and `requirements.txt` to a new repo on Github. Then sign into Streamlit and click on `Create App`. Specify the path to Github and modify the name.

![image](Deploy.png)

4. After successfully deploying the website, my app can be accessed at https://ids721-week9-9zicayrbzp9ess6oaswtsq.streamlit.app/.

![image](image.png)

## Deployment via AWS EC2
Following the video instructions, deployment can be achieved by running the following commands.
```bash
sudo apt update
```
```bash
sudo apt-get update
```
```bash
sudo apt upgrade -y
```
```bash
sudo apt install git curl unzip tar make sudo vim wget -y
```
```bash
git clone "https://gitlab.com/zibingao1010/week9.git"
```
```bash
sudo apt install python3-pip
```
```bash
pip3 install -r requirements.txt
```
```bash
#Temporary running
python3 -m streamlit run week9.py
```
```bash
#Permanent running
nohup python3 -m streamlit run week9.py
```